<?php

/**
 * @file
 * Flag module integration for Commerce Price Alerts.
 */

/**
 * Implements hook_flag_default_flags().
 */
function commerce_price_alerts_flag_default_flags() {
  $flags = array();

  // Exported flag: "Price alert".
  $flags[COMMERCE_PRICE_ALERTS_FLAG] = array(
    'entity_type' => 'node',
    'title' => 'Price alert',
    'global' => 0,
    'types' => array_keys(commerce_product_reference_node_types()),
    'flag_short' => 'Add Price Alert',
    'flag_long' => 'Subscribe to price drops',
    'flag_message' => "We'll Notify You When This Price Drops!",
    'unflag_short' => 'Remove Price Alert',
    'unflag_long' => 'Unsubscribe from price drops',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 1,
    'api_version' => 3,
  );
  return $flags;

}
