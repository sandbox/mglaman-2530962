<?php

/**
 * @file
 * Rules integration for Commerce Price Alerts.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_price_alerts_default_rules_configuration() {
  $rules = array();

  $rules['commerce_price_alerts_create_price_drop_alert_message'] = entity_import('rules_config', '{ "commerce_price_alerts_create_price_drop_alert_message" : {
    "LABEL" : "Create price drop alert message",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce Price Alerts" ],
    "REQUIRES" : [ "commerce_price", "rules", "entity" ],
    "ON" : { "commerce_product_presave" : [] },
    "IF" : [
      { "commerce_price_compare_price" : {
          "first_price" : [ "commerce-product:commerce-price" ],
          "operator" : "\u003C",
          "second_price" : [ "commerce-product-unchanged:commerce-price" ]
        }
      }
    ],
    "DO" : [
      { "entity_create" : {
          "USING" : {
            "type" : "message",
            "param_type" : "commerce_price_drop_alert",
            "param_user" : [ "commerce-product:creator" ]
          },
          "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
        }
      },
      { "data_set" : {
          "data" : [ "entity-created:message-commerce-product" ],
          "value" : [ "commerce-product" ]
        }
      },
      { "data_set" : { "data" : [ "entity-created:message-commerce-sent" ], "value" : "0" } },
      { "data_set" : {
          "data" : [ "entity-created:message-commerce-price" ],
          "value" : [ "commerce-product:commerce-price" ]
        }
      },
      { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } }
    ]
  }
}');

  return $rules;
}
