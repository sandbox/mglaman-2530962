<?php

/**
 * @file
 * Commerce price drop integration with Message
 */

/**
 * Implements hook_default_message_type().
 */
function commerce_price_alerts_default_message_type() {
  $item['commerce_price_drop_alert'] = entity_import('message_type', '{
    "name" : "' . COMMERCE_PRICE_ALERTS_MESSAGE_BUNDLE . '",
    "description" : "Commerce Price Alert: price drop",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "",
    "arguments" : null,
        "message_text" : { "und" : [
        {
          "value" : "@{message:message-commerce-product:title} at [site:name] price drop",
          "format" : null,
          "safe_value" : "@{message:message-commerce-product:title} at [site:name] price drop"
        },
        {
          "value" : "@{message:message-commerce-product:title} now has a reduced price of [message:message-commerce-price:amount_decimal]",
          "format" : null,
          "safe_value" : "@{message:message-commerce-product:title} now has a reduced price of [message:message-commerce-price:amount_decimal]"
        }
      ]
    },
    "rdf_mapping" : []
  }');
  return $item;
}

/**
 * Implements hook_default_message_type_alter().
 *
 * Use the LANGUAGE_NONE values for the other languages in multilingual sites.
 */
function commerce_price_alerts_default_message_type_alter(&$items) {
  if (module_exists('locale')) {
    $languages = locale_language_list();
    foreach ($languages as $langcode => $langname) {
      foreach ($items as $message_type => $item) {
        if ($item->module != 'commerce_price_alerts') {
          continue;
        }
        if (isset($items[$message_type]->message_text[LANGUAGE_NONE])) {
          $items[$message_type]->message_text[$langcode] = $items[$message_type]->message_text[LANGUAGE_NONE];
        }
        if (isset($items[$message_type]->message_order_display_name[LANGUAGE_NONE])) {
          $items[$message_type]->message_order_display_name[$langcode] = $items[$message_type]->message_order_display_name[LANGUAGE_NONE];
        }
      }
    }
  }
}

/**
 * Refresh the fields attached to the message types we support.
 */
function commerce_price_alerts_message_field_refresh() {
  $fields['message_commerce_product']['field'] = array(
    'type' => 'entityreference',
    'module' => 'entityreference',
    'cardinality' => '1',
    'translatable' => FALSE,
    'settings' => array(
      'target_type' => 'commerce_product',
      'handler' => 'base',
      'handler_settings' => array(
        'target_bundles' => array(),
        'sort' => array(
          'type' => 'property',
          'property' => 'order_id',
          'direction' => 'ASC',
        ),
      ),
    ),
    'locked' => TRUE,
  );
  $fields['message_commerce_product']['instance'] = array(
    'entity_type' => 'message',
    'bundle' => COMMERCE_PRICE_ALERTS_MESSAGE_BUNDLE,
    'label' => 'Product',
    'required' => TRUE,
    'widget' => array(
      'type' => 'entityreference_autocomplete',
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'path' => '',
      ),
    ),
    'settings' => array(),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'entityreference_label',
        'settings' => array(
          'link' => FALSE,
        ),
        'module' => 'entityreference',
        'weight' => 0,
      ),
    ),
  );

  $fields['message_commerce_sent']['field'] = array(
    'type' => 'list_boolean',
    'module' => 'list',
    'cardinality' => '1',
    'translatable' => FALSE,
    'settings' => array(
      'allowed_values' => array(
        0 => 'Not sent',
        1 => 'Sent',
      ),
    ),
    'locked' => TRUE,
  );
  $fields['message_commerce_sent']['instance'] = array(
    'entity_type' => 'message',
    'bundle' => COMMERCE_PRICE_ALERTS_MESSAGE_BUNDLE,
    'label' => 'Price alert sent',
    'widget' => array(
      'type' => 'options_onoff',
      'module' => 'options',
      'active' => 1,
    ),
    'settings' => array(),
    'display' => array(
      'default' => array(
        'type' => 'hidden',
      ),
    ),
  );

  // Create the missing fields.
  foreach ($fields as $field_name => $info) {
    $field = $info['field'];
    $field += array(
      'field_name' => $field_name,
    );
    if (!field_info_field($field_name)) {
      field_create_field($field);
    }

    $instance = $info['instance'];
    $instance['field_name'] = $field_name;
    if (!field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
      field_create_instance($instance);
    }
  }

  // Attach a Commerce Price instance so we have price snapshot.
  // @note: otherwise message will always display current referenced product's
  //        price, and not the one at time of message creation.
  commerce_price_create_instance('message_commerce_price', 'message', COMMERCE_PRICE_ALERTS_MESSAGE_BUNDLE, 'Price');
}

/**
 * Implements hook_field_access().
 */
function commerce_price_alerts_field_access($op, $field, $entity_type, $entity, $account) {
  if ($op == 'edit' && $field['field_name'] == 'message_commerce_product') {
    return FALSE;
  }
}
